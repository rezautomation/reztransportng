package com.rezgateway.automation;

import com.rezgateway.automation.pojo.HttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class HttpClientHandler {
	private String USER_AGENT = "Mozilla/5.0";

	public HttpResponse sendGet(String url)
			throws UnsupportedOperationException, IOException {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);
		request.addHeader("User-Agent", this.USER_AGENT);
		org.apache.http.HttpResponse response = client.execute(request);
		HttpResponse Response = new HttpResponse();
		Response.setRESPONSE_CODE(response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";

		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		Response.setRESPONSE(result.toString());
		return Response;
	}
}